export interface Article{
    id?:number;
    name:string;
    contenu:string;
    datePublication:string;
    auteur:string;
    id_categorie: number;
    image:string;
    nom:string;
}

export interface User{
    email:string;
    password:string;
    role:string;
}