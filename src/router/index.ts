import SingleArticleView from "@/views/SingleArticleView.vue";
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

import AddArticleView from "@/views/AddArticleView.vue";
import CategorieView from "@/views/CategorieView.vue";
import LoginViews from "@/views/LoginViews.vue";


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },

    {
      path: "/Categorie/:theme",
      name: "Categorie",
      component: CategorieView,
    },
    {
      path: "/article/:id",
      name: "SingleArticleView.vue",
      component: SingleArticleView,
    },

    {
      path: "/AddArticle",
      name: "AddArticleView",
      component: AddArticleView,
    },{
      path:"/login",
      name: "login",
      component: LoginViews,
    }
    

  ],
});

export default router;
