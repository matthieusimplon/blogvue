import axios from "axios";
import type { Article } from "@/entities"



export async function fetchAllArticles() {
    const response = await axios.get<Article[]>('http://localhost:8080/api/article');
    return response.data;
}
export async function fetchOneArticle(id:any) {
    const response = await axios.get<Article>('http://localhost:8080/api/article/'+id);
    return response.data;
}
export async function postArticle(article:Article) {
    const response = await axios.post<Article>('http://localhost:8080/api/article', article);
    return response.data;
}


export async function fetchAllArticlesByCategorie( id:any){
    const response = await axios.get<Article[]>('http://localhost:8080/api/article/categorie/'+id);
    return response.data;
}

export async function fetchAllLasArticles() {
    const response = await axios.get<Article[]>('http://localhost:8080/api/article/last');
    return response.data;
}
